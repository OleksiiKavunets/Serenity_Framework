package serenity.steps;

import core.common.pages.ProductDetailsPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class ProductDetailsPageSteps extends ScenarioSteps{

    private ProductDetailsPage productDetailsPage;

    public ProductDetailsPageSteps(final Pages pages){
        productDetailsPage = pages.getPage(ProductDetailsPage.class);
    }

    @Step
    public void setQty(final String qty){

    }

    @Step
    public void setSize(final String size){

    }

    @Step
    public void clickAddToCartButton(){

    }






}
