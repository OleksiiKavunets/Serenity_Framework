package serenity.steps;

import core.common.pages.HomePage;
import core.common.utils.WebDriverUtil;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.Pages;
import net.thucydides.core.steps.ScenarioSteps;

public class HomePageSteps extends ScenarioSteps {

    private HomePage homePage;

    public HomePageSteps(final Pages pages){
        homePage = pages.getPage(HomePage.class);
    }

    @Step
    public void openProductDetailsPage(final String partialPath){
        homePage.openPageByExtraPath(HomePage.class, partialPath);
        WebDriverUtil.waitForAsyncExecution();
    }
}
