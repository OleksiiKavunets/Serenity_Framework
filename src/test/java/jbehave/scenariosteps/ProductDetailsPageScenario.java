package jbehave.scenariosteps;

import net.thucydides.core.annotations.Steps;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import serenity.steps.HomePageSteps;
import serenity.steps.ProductDetailsPageSteps;

public class ProductDetailsPageScenario {

    @Steps
    private HomePageSteps homePageSteps;

    @Steps
    private ProductDetailsPageSteps productDetailsPageSteps;

    @Given("user has opened product detais page: $partialLink")
    @Alias("user has opened product detais page: $partialLink")
    public void navigateToProductDetailsPage(final String partialUrl){
        homePageSteps.openProductDetailsPage(partialUrl);
    }

    @When("user fill in following parameters: $productParameters")
    public void fillProductParameters(final ExamplesTable parameters){
//        parameters
        productDetailsPageSteps.setSize("");
        productDetailsPageSteps.setQty("");

    }

    @When("clicks 'ADD TO CART' button")
    public void clickAddToCartButton(){
        productDetailsPageSteps.clickAddToCartButton();
    }




}
