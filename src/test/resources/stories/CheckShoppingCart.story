Meta:
@Shopping Cart

Narrative:
In order to buy a product
As a user
I want to be able to see selable product added to Shopping Cart Page

Scenario: Check add product to Shopping Cart

Given user has opened product detais page: /categories/men/footwear/training-shoes/product/adidas-mens-athletics-247-training-shoes-dark-greywhite-332282849.html
When user fill in following parameters:
| size | qty |
| 10   | 2   |
And clicks 'ADD TO CART' button
And navigate to Shopping Cart Page
Then following product should be added to Shopping Cart:
| title                                                        | qty | price  | total   |
| adidas Men's Athletics 24/7 Training Shoes - Dark Grey/White | 2   | $83.96 | $167.92 |
