package core.common.pages;

import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.WebDriver;

@DefaultUrl("https://www.sportchek.ca")
public class HomePage extends AbstractPage{

    protected HomePage(final WebDriver driver) {
        super(driver);
    }
}
